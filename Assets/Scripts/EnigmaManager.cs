﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnigmaManager : MonoBehaviour
{
	//instance of the singleton pattern
	public static EnigmaManager instance;

	//object that the engima scene will interact with
	public GameObject enigmaWall;
	public List<GameObject> schemes;
	public List<GameObject> lights;
	public GameObject treasureWall;

	//variables to manage the player rotation around schemes
	float duration = 3.0f;
	float progression = 1.1f;
	Quaternion initRotation;
	Quaternion targetRotation;

	//controller to access our player and unpause when engima scene is finished
	RailShooterController controller;

	void Awake()
	{
		//singleton pattern
		if (instance == null)
			instance = this;
		else if (instance != this)
			Destroy(this.gameObject);
	}

	private void Update()
	{
		//if we need to rotate, rotate towards targetRotation
		if(progression <= 1.0f)
		{
			controller.transform.rotation = Quaternion.Lerp(initRotation, targetRotation, progression);
			progression += Time.deltaTime / duration;
		}
	}

	//coroutine that manage the enigma scene
	public IEnumerator EnigmaCoroutine(RailShooterController rsc)
	{
		controller = rsc;

		enigmaWall.SetActive(true);

		//activate scheme 1, rotate towards it and wait for the scheme to be completed
		schemes[0].SetActive(true);
		lights[0].SetActive(true);
		initRotation = controller.transform.rotation;
		targetRotation = Quaternion.Euler(0.0f, 180.0f, 0.0f);
		progression = 0.0f;
		yield return new WaitUntil(() => progression > 1.0f);
		yield return new WaitUntil(() => schemes[0].GetComponent<Scheme>().schemeDone == true);
		schemes[0].SetActive(false);
		lights[0].GetComponent<Light>().intensity = 10;

		//activate scheme 2, rotate towards it and wait for the scheme to be completed
		schemes[1].SetActive(true);
		lights[1].SetActive(true);
		initRotation = controller.transform.rotation;
		targetRotation = Quaternion.Euler(0.0f, 90.0f, 0.0f);
		progression = 0.0f;
		yield return new WaitUntil(() => progression > 1.0f);
		yield return new WaitUntil(() => schemes[1].GetComponent<Scheme>().schemeDone == true);
		schemes[1].SetActive(false);
		lights[1].GetComponent<Light>().intensity = 10;

		//activate scheme 3, rotate towards it and wait for the scheme to be completed
		schemes[2].SetActive(true);
		lights[2].SetActive(true);
		initRotation = controller.transform.rotation;
		targetRotation = Quaternion.Euler(0.0f, -90.0f, 0.0f);
		progression = 0.0f;
		yield return new WaitUntil(() => progression > 1.0f);
		yield return new WaitUntil(() => schemes[2].GetComponent<Scheme>().schemeDone == true);
		schemes[2].SetActive(false);
		lights[2].GetComponent<Light>().intensity = 10;

		//activate scheme 4, rotate towards it and wait for the scheme to be completed
		schemes[3].SetActive(true);
		lights[3].SetActive(true);
		initRotation = controller.transform.rotation;
		targetRotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
		progression = 0.0f;
		yield return new WaitUntil(() => progression > 1.0f);
		yield return new WaitUntil(() => schemes[3].GetComponent<Scheme>().schemeDone == true);
		schemes[3].SetActive(false);
		lights[3].GetComponent<Light>().intensity = 10;

		//end of the engima scene to prepare the end scene
		Destroy(GameObject.Find("BGM"));
		EndManager.instance.GetComponent<AudioSource>().Play();
		treasureWall.GetComponent<Animation>().Play();
		yield return new WaitForSeconds(4.0f);
		controller.waiting = false;
	}
}
