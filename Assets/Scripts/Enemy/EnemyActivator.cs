﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyActivator : MonoBehaviour
{
	public bool awake = false;

	private void OnTriggerEnter(Collider other)
	{
		//player detection
		if (other.CompareTag("Player"))
			awake = true;
	}
}
