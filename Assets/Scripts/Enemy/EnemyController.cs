﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
	public EnemyActivator activator;
	public float speed;

    void FixedUpdate()
    {
		//if player has been detected at the activator position, move
		if (activator.awake)
			transform.position += transform.forward * speed * Time.fixedDeltaTime;
    }

	//coroutine for simulate knockback
	public IEnumerator KnockbackCoroutine(CapsuleCollider coll)
	{
		speed = -speed * 2;
		coll.enabled = false;
		GetComponent<Animator>().SetTrigger("Hit");
		yield return new WaitForSeconds(0.3f);
		speed = -speed / 2;
		yield return new WaitForSeconds(0.33f);
		coll.enabled = true;
	}
}
