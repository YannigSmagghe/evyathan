﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyUnit : MonoBehaviour
{

    public int health;
    private int currentHealth;

    public int damageSword = 10;
    public int damageFireball = 20;
    public int damageIceball = 40;
    public int damagePoisonball = 40;
    public int damageArcaneball = 100;
    // Start is called before the first frame update
    void Start()
    {
        currentHealth = health;
    }

    // Update is called once per frame
    void Update()
    {
        if (currentHealth <= 0)
        {
            Destroy(transform.parent.gameObject);
        }

    }

    // Trigger enemy to damage deal
    void OnTriggerEnter(Collider other)
    {
       
        Transform spellParent = other.gameObject.transform.parent;
		if (other.CompareTag("Sword"))
		{
			TakeDamage(damageSword);
		}
		else if (other.CompareTag("Player"))
		{
			other.GetComponent<RailShooterController>().hp = 0;
		}
		else if (spellParent.CompareTag("Fireball"))
        {
            TakeDamage(damageFireball);
        }
        else if (spellParent.CompareTag("Iceball") && gameObject.CompareTag("Wall"))
        {
            TakeDamage(damageIceball);
        }
        else if (spellParent.CompareTag("Poisonball"))
        {
            TakeDamage(damagePoisonball);
        }
        else if (spellParent.CompareTag("Arcaneball"))
        {
            TakeDamage(damageArcaneball);
        }
        

    }

    private void TakeDamage(int damage)
    {
        currentHealth -= damage;
		StartCoroutine(GetComponentInParent<EnemyController>().KnockbackCoroutine(GetComponent<CapsuleCollider>()));
    }
}
