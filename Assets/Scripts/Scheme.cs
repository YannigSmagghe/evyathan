﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scheme : MonoBehaviour
{
	//points of the scheme that need to be activated
    public List<GameObject> schemeCheckList;
	[HideInInspector]
    public int currentIndex = 0;

	//material to activate the points with
	public Material checkedMat;

	[HideInInspector]
	public bool schemeDone = false;
	
    void Update()
    {
		//if we're not at the end, activate the next point
        if (currentIndex < schemeCheckList.Count)
        {
			schemeCheckList[currentIndex].SetActive(true);
        }

		//scheme done, all points activated
        if (currentIndex == schemeCheckList.Count)
        {
			schemeDone = true;
        }

    }
}
