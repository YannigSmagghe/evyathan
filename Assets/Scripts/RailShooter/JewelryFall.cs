﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JewelryFall : MonoBehaviour
{
	//objects that will be spawn with their different materials
	public List<GameObject> jewels;
	public List<Material> materials;

	public float minTimer;
	public float maxTimer;
	public int minJewelSpawn;
	public int maxJewelSpawn;

	float timer;

	void Start()
	{
		//random timer
		timer = Random.Range(minTimer, maxTimer);
	}

	void Update()
    {
		timer -= Time.deltaTime;
		if(timer <= 0.0f)
		{
			//random amount of gem that will be spawn
			int amount = Random.Range(minJewelSpawn, maxJewelSpawn);
			//instantiate the gems at a spawn position
			for(int i = amount; i > 0; i--)
				Instantiate(randomJewel(), spawnPosition(transform.position), Quaternion.identity, transform);
			//reset timer
			timer = Random.Range(minTimer, maxTimer);
		}
    }

	//select a random jewel and give it a random material
	GameObject randomJewel()
	{
		GameObject selected = jewels[Random.Range(0, jewels.Count)];
		selected.GetComponentInChildren<MeshRenderer>().material = materials[Random.Range(0, materials.Count)];

		return selected;
	}

	//return a position on a circle around initPos with some restriction (to spawn our gems only on the treasure in the last room)
	Vector3 spawnPosition(Vector3 initPos)
	{
		Vector2 circlePos = Random.insideUnitCircle;
		circlePos = circlePos.normalized;
		if (circlePos.y < 0.0f && circlePos.x > -0.5f && circlePos.x < 0.5f)
			circlePos.y = -circlePos.y;

		return new Vector3(initPos.x + circlePos.x * Random.Range(9.0f, 12.0f), initPos.y, initPos.z + circlePos.y * Random.Range(6.0f, 9.0f));
	}
}
