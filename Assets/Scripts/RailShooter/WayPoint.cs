﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

//type of the differents waypoint we will have in the level, all explained in RailShooterController
public enum WPType {normal, checkpoint, choice, enigma, end};

public class WayPoint : MonoBehaviour
{
	public WayPoint next;

	public WPType type;

	//for choice waypoints
	public List<WayPoint> choiceWayPoints;

	//for checkpoints waypoints
	public GameObject content;
}
