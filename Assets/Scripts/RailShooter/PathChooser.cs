﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathChooser : MonoBehaviour
{
	[HideInInspector]
	public WayPoint actual;
	public bool left;

	private void OnTriggerEnter(Collider other)
	{
		//if the player has choosed his way with his sword, put the good waypoint as the next one
		if (other.CompareTag("Sword"))
			actual.next = left ? actual.choiceWayPoints[0] : actual.choiceWayPoints[1];
	}
}
