﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RailShooterController : MonoBehaviour
{
	WayPoint actual;
	WayPoint checkpoint;

	public float speed;
	public float rotationSpeed;
	float progression, step;

	public int baseHP;
	[HideInInspector]
	public int hp;

	public GameObject choicePanel;

	public bool waiting=false;

	GameObject currentContent;
	
    void Start()
    {
		//get the starting point of the level
		actual = GameObject.Find("StartPoint").GetComponent<WayPoint>();
		checkpoint = actual;

		transform.position = actual.transform.position;

		//preparation for the lerp
		progression = 0.0f;
		step = speed / (actual.next.transform.position - actual.transform.position).magnitude;

		hp = baseHP;

		//instantiate next group of ennemies
		currentContent = Instantiate(checkpoint.content, GameObject.Find("Enemies").transform);
	}
	
    void FixedUpdate()
    {
		if (!waiting)
		{
			if (hp <= 0)
			{
				//respawn at checkpoint
				actual = checkpoint;
				progression = 0.0f;
				step = speed / (actual.next.transform.position - actual.transform.position).magnitude;
				hp = baseHP;
				//reset actual group of enemies
				Destroy(currentContent);
				currentContent = Instantiate(checkpoint.content, GameObject.Find("Enemies").transform);
			}
			//if we're not arrived at the next waypoint
			if (progression < 1.0f)
			{
				//move slightly toward the next waypoint and look at it
				progression += step * Time.deltaTime;
				transform.position = Vector3.LerpUnclamped(actual.transform.position, actual.next.transform.position, progression);
				transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation((actual.next.transform.position - actual.transform.position)), rotationSpeed * Time.deltaTime);
			}
			else //if wer're on the next waypoint
			{
				progression -= 1.0f;
				//depending the type of the waypoint...
				switch (actual.next.type)
				{
					case WPType.normal:
						//just pass to the next one
						actual = actual.next == null ? actual : actual.next;
						break;
					case WPType.checkpoint:
						//change checkpoint, instantiate the next group of enemies and pass to the next waypoint
						checkpoint = actual.next;
						Destroy(currentContent);
						currentContent = Instantiate(checkpoint.content, GameObject.Find("Enemies").transform);
						actual = actual.next == null ? actual : actual.next;
						break;
					case WPType.choice:
						//start the choice coroutine, and pause this script to wait for the player choice (left or right)
						actual = actual.next;
						StartCoroutine(ChoiceCoroutine(actual));
						waiting = true;
						break;
					case WPType.enigma:
						//start the enigma coroutine that will manage the "enigma" in the last room
						StartCoroutine(EnigmaManager.instance.EnigmaCoroutine(this));
						waiting = true;
						actual = actual.next == null ? actual : actual.next;
						step = speed / (actual.next.transform.position - actual.transform.position).magnitude;
						break;
					case WPType.end:
						//start the end coroutine that will manage the end scene of the gam when we enter the treasure room
						StartCoroutine(EndManager.instance.EndSceneCoroutine());
						waiting = true;
						break;
				}
				//calculate the step for the lerp depending the distance between actual and next waypoints
				if(!waiting)
					step = speed / (actual.next.transform.position - actual.transform.position).magnitude;
			}
		}
    }

	//coroutine launched when the player is on an intersection, to let him choose his way
	public IEnumerator ChoiceCoroutine(WayPoint actual)
	{
		PathChooser[] choosers = choicePanel.GetComponentsInChildren<PathChooser>();
		foreach (var chooser in choosers)
			chooser.actual = actual;
		choicePanel.SetActive(true);
		yield return new WaitUntil(() => actual.next != null);
		step = speed / (actual.next.transform.position - actual.transform.position).magnitude;
		choicePanel.SetActive(false);
		waiting = false;
	}
}
