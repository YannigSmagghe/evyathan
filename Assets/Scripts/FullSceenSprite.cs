﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FullSceenSprite : MonoBehaviour
{
    public GameObject sprite;
    public GameObject Camera;
    public int width;
    public int height;

    void Update()
    {
        ResizeSpriteToScreen(sprite, Camera.GetComponent<Camera>(), width, height);
    }

    // If fitToScreenWidth is set to 1 then the width fits the screen width.
    // If it is set to anything over 1 then the sprite will not fit the screen width, it will be divided by that number.
    // If it is set to 0 then the sprite will not resize in that dimension.
    void ResizeSpriteToScreen(GameObject theSprite, Camera theCamera, int fitToScreenWidth, int fitToScreenHeight)
    {
        Camera camera = theCamera.GetComponent<Camera>();
        float spriteHeight = theSprite.GetComponent<SpriteRenderer>().sprite.bounds.size.y;
        float spriteWidth = theSprite.GetComponent<SpriteRenderer>().sprite.bounds.size.x;
        float distance = transform.position.z - camera.transform.position.z;
        float screenHeight = 2 * Mathf.Tan(camera.fieldOfView * Mathf.Deg2Rad / 2) * distance;
        float screenWidth = screenHeight * camera.aspect;
        transform.localScale = new Vector3(screenWidth / spriteWidth, screenHeight / spriteHeight, 1f);
    }
}
