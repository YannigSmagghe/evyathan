﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ImgProcessing))]
public class CastSpell : MonoBehaviour
{
    private ImgProcessing imgProcessingManager;
    // Start is called before the first frame update
    void Start()
    {
        imgProcessingManager = GameObject.FindGameObjectWithTag("ImgProcessingManager").GetComponent<ImgProcessing>();
    }

    // Active one spell when marker is detected
    void Update()
    {
        if (imgProcessingManager._ice_ball_detected)
        {
            CastThisSpell(this.transform.Find("Iceball"));
        }

        if (imgProcessingManager._fire_ball_detected)
        {
            CastThisSpell(this.transform.Find("Fireball"));
        }

        if (imgProcessingManager._poison_ball_detected)
        {
            CastThisSpell(this.transform.Find("Poisonball"));
        }

        if (imgProcessingManager._arcane_ball_detected)
        {
            CastThisSpell(this.transform.Find("Arcaneball"));
        }
    }

    private void CastThisSpell(Transform find)
    {
        find.gameObject.SetActive(true);
    }
}
