﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveSword : MonoBehaviour
{
	private GameObject swordManager;
    // This collider is used to interact with the world (enemies and puzzles)
	private SphereCollider coll;
	// Speed in units per sec.
	public float speed;
    // Coordinates from image processing (in percentage of screen, from -1 to 1)
	public float factorImgProcessingX;
	public float factorImgProcessingY;
    // To attack, you must move your sword, not just point at things
	public float attackMovementTreshold;

	public Transform hand;

	// Start is called before the first frame update
	void Start()
	{
		
		coll = GetComponent<SphereCollider>();
	}

	// Update is called once per frame
	void Update()
	{
		GetSwordTrackPosition();
		if (swordManager)
		{
			Vector2 trackPosition = ConvertUnitToWorldSize(swordManager.GetComponent<ImgProcessing>()._greenRef_position);
			SmoothMove(trackPosition);
		}
	}

	private void SmoothMove(Vector2 trackPosition)
	{
		// The step size is equal to speed times frame time.
		float step = speed * Time.fixedDeltaTime;

		//activate the collider of the sword only if the movement is sufficient
		float movement = (transform.localPosition - new Vector3(trackPosition.x, trackPosition.y, 2.75f)).magnitude;
		coll.enabled = movement >= attackMovementTreshold ? true : false;

		// Move our position a step closer to the target.
		transform.localPosition = Vector3.MoveTowards(transform.localPosition, new Vector3(trackPosition.x, trackPosition.y, 2.75f), step); 
		transform.LookAt(hand.position);
	}

    // Getting sword manager
	private void GetSwordTrackPosition()
	{
		GameObject[] swordGameObjectsArray = GameObject.FindGameObjectsWithTag("ImgProcessingManager");
		if (swordGameObjectsArray[0] != null)
		{
			swordManager = swordGameObjectsArray[0];
		}
	}

    // Sword position must be convert from image processing to world space
    // The sword manager gives a percentage of the webcam image
	private Vector2 ConvertUnitToWorldSize(Vector2 inputRef)
	{
		inputRef = new Vector2(factorImgProcessingX * inputRef.x, factorImgProcessingY * inputRef.y);
		return inputRef;
	}


}
