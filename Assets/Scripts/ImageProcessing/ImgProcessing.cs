﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Emgu.CV;
using Emgu.CV.Aruco;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.Drawing;
using System.IO;

public class ImgProcessing : MonoBehaviour
{
    #region attributs

    #region Debug settings
    public bool _display_canvas;
    public bool _debug_window;
    public bool _enable_color_detection;
    public bool _enable_marker_detection;
    public bool _enable_cat_detection;
    #endregion

    #region Gameplay variables
    [Header("Gameplay variables")]
    /*Position are in percentage of frame size: -1,-1 (bottom left) to 1,1 (up right)*/
    public Vector2 _greenRef_position;
    public bool _arcane_ball_detected;
    public bool _fire_ball_detected;
    public bool _ice_ball_detected;
    public bool _poison_ball_detected;
    public bool _cat_face_detected;
    #endregion

    #region Webcam settings
    [Header("Webcam settings")]
    public int _webcamIdNumber = 0;
    private VideoCapture _webcam;
    private Mat _webcamFrame;
    #endregion

    #region Frame settings
    private string _window = "Detection window";
    private Mat _workingFrame;
    private Mat _displayFrame;
    #endregion

    #region Display in unity settings
    [Header("Display in unity")]
    public UnityEngine.UI.Image _ui_display;
    private Texture2D _tex_display;
    #endregion

    #region Color settings
    [Header("Filters settings")]
    [Range(1, 10)]
    public int _medianBlurFactor;
    [Range(1, 10)]
    public int _gaussianBlurFactor;

    [Header("Color triggers settings")]
    [Range(0, 255)]
    public double _hueLowTrigger;
    [Range(0, 255)]
    public double _hueHighTrigger;
    [Range(0, 255)]
    public double _saturationLowTrigger;
    [Range(0, 255)]
    public double _saturationHighTrigger;
    [Range(0, 255)]
    public double _valueLowTrigger;
    [Range(0, 255)]
    public double _valueHighTrigger;
    [Range(0, 800)]
    public double _minContourArea;

    [Header("Morphology settings")]
    [Range(0, 10)]
    public int _morphingElementSize;
    [Range(1, 10)]
    public int _morphingIteration;
    #endregion

    #region Aruco settings
    Dictionary ArucoDict;
    DetectorParameters ArucoParameters;
    #endregion

    #region Cat detection settings
    [Header("Cat detection settings")]
    public string _filepath_catFacesClassifer = "/Resources/lbpcascade_frontalcatface.xml";
    private CascadeClassifier _frontFacesCascadeClassifier;

    [Range(10, 100)]
    public int MIN_FACE_SIZE = 30;
    [Range(50, 300)]
    public int MAX_FACE_SIZE = 100;

    private Rectangle[] _catFaces;
    #endregion

    #region ROI settings
    private Rectangle _roi_marker;
    private Rectangle _roi_spell_launcher;
    #endregion
    #endregion

    void Start()
    {
        #region Initialize webcam parameters
        _webcam = new VideoCapture(_webcamIdNumber);
        _webcamFrame = new Mat();
        _webcam.ImageGrabbed += new EventHandler(_handleWebcamQueryFrame);
        #endregion

        #region Initialize frames
        _workingFrame = new Mat();
        _displayFrame = new Mat();
        #endregion

        #region Initialize Aruco dictionary and parameters for markers detection
        ArucoDict = new Dictionary(Dictionary.PredefinedDictionaryName.Dict4X4_50); // bits x bits (per marker) _ number of markers in dict
        ArucoParameters = new DetectorParameters();
        ArucoParameters = DetectorParameters.GetDefault();
        #endregion

        #region Initialize Cat Faces
        _frontFacesCascadeClassifier = new CascadeClassifier(fileName: Application.dataPath + _filepath_catFacesClassifer);
        #endregion

        #region Initialize Gameplay variables
        _arcane_ball_detected = false;
        _fire_ball_detected = false;
        _ice_ball_detected = false;
        _poison_ball_detected = false;
        _cat_face_detected = false;
        #endregion

        _webcam.Start();   // starting footage acquisition
    }

    void Update()
    {
        //if (_webcam.IsOpened)
           // _webcam.Grab();

        if (_display_canvas)
            DisplayFrameInTexture(ref _displayFrame);

        if (_debug_window)
            if (!_workingFrame.IsEmpty) CvInvoke.Imshow(_window, _displayFrame);
    }

    private void OnDestroy()
    {
        // stoping footage acquisition, dispose camera and destroy all windows
        _webcam.Stop();                 
        _webcam.Dispose();
        CvInvoke.DestroyAllWindows();
    }

    // Event handler, called each time an image is grabbed
    private void _handleWebcamQueryFrame(object sender, EventArgs e)
    {
        if (_webcam.IsOpened) _webcam.Retrieve(_webcamFrame);
        if (_webcamFrame.IsEmpty) return;
        FrameProcessing();
    }

    // Image processing
	private void FrameProcessing()
	{
		_workingFrame = _webcamFrame.Clone();

        // region of interest, to process only half of the image
		_roi_marker = new Rectangle(_workingFrame.Width / 2, 0, _workingFrame.Width / 2, _workingFrame.Height);
		_roi_spell_launcher = new Rectangle(0, 0, _workingFrame.Width / 2, _workingFrame.Height);

        // marker detection: needs to be done BEFORE the flip (to match the dictionnary)
        // this is only the cloning step, the process is later
		Mat imgMarker = new Mat(_workingFrame, _roi_marker);
		imgMarker = imgMarker.Clone();

        // Flip to display as a mirror
		CvInvoke.Flip(_workingFrame, _workingFrame, FlipType.Horizontal);

        // Cloning image for processing
		Mat imgColor = _workingFrame.Clone();
		Mat imgCat = new Mat(_workingFrame, _roi_spell_launcher);

		if (_enable_color_detection)
			ColorDetection(ref imgColor);

		if (_enable_marker_detection)
			MarkerDetection(ref imgMarker);

		if (_enable_cat_detection)
			CatDetection(ref imgCat);

		_displayFrame = _workingFrame.Clone();
	}

	private void ColorDetection(ref Mat img)
    {
        if (!img.IsEmpty)
        {

            VectorOfVectorOfPoint contours = ContoursFromThresholdingColor(ref img);
            VectorOfPoint contour = ImgProcessingLib.BiggestContour(ref contours, _minContourArea);
            int? contourIndex = ImgProcessingLib.BiggestContourIndex(ref contours, _minContourArea);
            Point center = ImgProcessingLib.ContourCenter(ref contour);

            if (contourIndex != null)
                CvInvoke.DrawContours(_workingFrame, contours, (int)contourIndex, new MCvScalar(0, 0, 255), 3);
            if (!center.IsEmpty)
            {
                CvInvoke.Circle(_workingFrame, center, 3, new MCvScalar(0, 0, 255), 2);
                _greenRef_position = ConvertPixelPos(center, img.Width, img.Height);
            }
        }
    }

    private void MarkerDetection(ref Mat img)
    {
        if (!img.IsEmpty)
        {

            #region Detect markers on last retrieved frame
            VectorOfInt ids = new VectorOfInt(); // name/id of the detected markers
            VectorOfVectorOfPointF corners = new VectorOfVectorOfPointF(); // corners of the detected marker
            VectorOfVectorOfPointF rejected = new VectorOfVectorOfPointF(); // rejected contours
            ArucoInvoke.DetectMarkers(img, ArucoDict, corners, ids, ArucoParameters, rejected);

            #region Update gameplay variables
            // You only need to detect one marker to launch a spell
            _arcane_ball_detected = false;
            _fire_ball_detected = false;
            _ice_ball_detected = false;
            _poison_ball_detected = false;

            for (int i = 0; i < ids.Size; ++i)
            {
                InterpreteMarkerId(ids[i]);
            }
            #endregion

            if (ids.Size > 0)
            {
                #region Draw detected markers
                // Flip and offset to match the display
                corners = ImgProcessingLib.HorizontalFlipCorners(ref corners, _workingFrame.Width);
                corners = ImgProcessingLib.OffsetCorners(ref corners, new PointF(- _webcamFrame.Width / 2, 0));
                ArucoInvoke.DrawDetectedMarkers(_workingFrame, corners, ids, new MCvScalar(255, 0, 255));
                #endregion
            }
            #endregion
        }
    }


    private void CatDetection(ref Mat img)
    {
        Mat _webcamFrameGray = img.Clone();

        CvInvoke.CvtColor(_webcamFrameGray, _webcamFrameGray, ColorConversion.Bgr2Gray);
        if (_webcamFrameGray.IsEmpty) return;

        // Using a classifier to detect a cat face
        _catFaces = _frontFacesCascadeClassifier.DetectMultiScale(_webcamFrameGray, minSize: new Size(MIN_FACE_SIZE, MIN_FACE_SIZE), maxSize: new Size(MAX_FACE_SIZE, MAX_FACE_SIZE), minNeighbors: 5);

        _cat_face_detected = (_catFaces != null) && (_catFaces.Length > 0);

        for (int i = 0; i < _catFaces.Length; i++)
        {
            CvInvoke.Rectangle(_workingFrame, _catFaces[i], new MCvScalar(0, 0, 255), thickness: 3);
            
        }
    }

    private VectorOfVectorOfPoint ContoursFromThresholdingColor(ref Mat img)
    {
        Mat imgHSV = img.Clone();
        CvInvoke.CvtColor(imgHSV, imgHSV, ColorConversion.Bgr2Hsv);

        // Blur to remove noise
        imgHSV = ImgProcessingLib.FilterMedianBlur(ref imgHSV, _medianBlurFactor);
        imgHSV = ImgProcessingLib.FilterGaussianBlur(ref imgHSV, _gaussianBlurFactor, 1);

        Hsv hsvLowTrigger = new Hsv(_hueLowTrigger, _saturationLowTrigger, _valueLowTrigger);
        Hsv hsvHighTrigger = new Hsv(_hueHighTrigger, _saturationHighTrigger, _valueHighTrigger);
        Mat imgGray = ImgProcessingLib.ColorThresholdOnHSV(ref imgHSV, hsvLowTrigger, hsvHighTrigger);

        // Morphing to smooth contours
        imgGray = ImgProcessingLib.MorphingOpening(ref imgGray, _morphingElementSize, _morphingIteration);
        
        VectorOfVectorOfPoint contours = ImgProcessingLib.Contours(ref imgGray);
        
        return contours;
    }

    public void DisplayFrameInTexture(ref Mat img)
    {
        if (!img.IsEmpty)
        {
            Destroy(_tex_display);

            _tex_display = ImgProcessingLib.ConvertMatToTexture2D(ref img,
                (int)_ui_display.rectTransform.rect.width,
                (int)_ui_display.rectTransform.rect.height);

            _ui_display.sprite = Sprite.Create(_tex_display,
                new Rect(0.0f, 0.0f, _tex_display.width, _tex_display.height),
                new Vector2(0.5f, 0.5f),
                100.0f);
        }
    }

    // Convert pixel position from webcam pixel size to a range between -1 and 1 (ease the use in game)
    private static Vector2 ConvertPixelPos(Point point, int imgWidth, int imgHeight)
    {
        float x_pos = (float)(2 * point.X - imgWidth) / imgWidth;
        float y_pos = (float)(imgHeight - 2 * point.Y) / imgHeight;
        Vector2 position = new Vector2(x_pos, y_pos);
        return position;
    }

    // Marker translation to spells, each spell correspond to a group of 4 markers
    private void InterpreteMarkerId(int id)
    {
        switch (id)
        {
            case 0:
                _ice_ball_detected = true;
                break;
            case 1:
                _ice_ball_detected = true;
                break;
            case 2:
                _poison_ball_detected = true;
                break;
            case 3:
                _poison_ball_detected = true;
                break;
            case 4:
                _ice_ball_detected = true;
                break;
            case 5:
                _ice_ball_detected = true;
                break;
            case 6:
                _poison_ball_detected = true;
                break;
            case 7:
                _poison_ball_detected = true;
                break;
            case 8:
                _arcane_ball_detected = true;
                break;
            case 9:
                _arcane_ball_detected = true;
                break;
            case 10:
                _fire_ball_detected = true;
                break;
            case 11:
                _fire_ball_detected = true;
                break;
            case 12:
                _arcane_ball_detected = true;
                break;
            case 13:
                _arcane_ball_detected = true;
                break;
            case 14:
                _fire_ball_detected = true;
                break;
            case 15:
                _fire_ball_detected = true;
                break;
        }
    }
}