﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Emgu.CV;
using Emgu.CV.Aruco;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.Drawing;
using System.IO;

public class Markers : MonoBehaviour
{

    [Header("Webcam and window settings")]
    public int _webcamIdNumber = 0;
    private string _window = "Faces detection";
    private VideoCapture _webcam;
    private Mat _webcamFrame;
    private Mat _frameDisplay;
    private Mat _frameGray;

    [Header("Thresholding settings")]
    public bool _adaptive_threshold;
    public bool _adaptive_gaussian_threshold;
    [Range(0,255)]
    public double _threshold_value;
    [Range(0, 255)]
    public double _threshold_max_value;
    [Range(1, 10)]
    public int _threshold_bloc_size;
    [Range(-10, 10)]
    public double _threshold_param1;


    [Header("Filters settings")]
    public bool _activateMedianBlur;
    [Range(1, 10)]
    public int _medianBlurFactor;

    public bool _activateGaussianBlur;
    [Range(1, 10)]
    public int _gaussianBlurFactor;
    [Range(0.8f, 1.2f)]
    public float _gaussianBlurSigmaFactor;

    [Header("Morphology settings")]
    [Range(0, 10)]
    public int _morphingOperationSize;
    [Range(1, 10)]
    public int _morphingIteration;
    public bool _morphingOpening;
    public bool _morphingClosing;

    [Header("Contours settings")]
    public bool _enableContours;
    [Range(0, 1)]
    public double _minContourAreaProportion;
    [Range(0, 1)]
    public double _maxContourAreaProportion;
    [Range(0, 1)]
    public double _areaRectTolerance;
    [Range(0, 1)]
    public double _squareShapeTolerance;

    

    [Header("Aruco settings")]
    [Range(1, 20)]
    public double _AdaptiveThreshConstant = 7;
    [Range(2, 40)]
    public int _AdaptiveThreshWinSizeMax = 23;
    [Range(1, 20)]
    public int _AdaptiveThreshWinSizeMin = 3;
    [Range(1, 20)]
    public int _AdaptiveThreshWinSizeStep = 10;
    [Range(2, 40)]
    public int _CornerRefinementMaxIterations = 30;
    [Range(0, 1)]
    public double _CornerRefinementMinAccuracy = 0.1;
    [Range(1, 20)]
    public int _CornerRefinementWinSize = 5;
    [Range(0, 1)]
    public double _ErrorCorrectionRate = 0.6;
    public int _MarkerBorderBits = 1;
    [Range(0, 1)]
    public double _MaxErroneousBitsInBorderRate = 0.35;
    [Range(1, 20)]
    public double _MaxMarkerPerimeterRate = 4.0;
    [Range(0, 1)]
    public double _MinCornerDistanceRate = 0.05;
    [Range(1, 20)]
    public int _MinDistanceToBorder = 3;
    [Range(0, 1)]
    public double _MinMarkerDistanceRate = 0.05;
    [Range(0, 1)]
    public double _MinMarkerPerimeterRate = 0.03;
    [Range(1, 20)]
    public double _MinOtsuStdDev = 5.0;
    [Range(0, 1)]
    public double _PerspectiveRemoveIgnoredMarginPerCell = 0.13;
    [Range(1, 20)]
    public int _PerspectiveRemovePixelPerCell = 8;

    Dictionary ArucoDict;
    DetectorParameters ArucoParameters;

    void Start()
    {
        _webcam = new VideoCapture(_webcamIdNumber);
        _webcamFrame = new Mat();
        _frameDisplay = new Mat();
        _frameGray = new Mat();
        _webcam.ImageGrabbed += new EventHandler(_handleWebcamQueryFrame);

        #region Initialize and save Aruco dictionary

        //default dictionary
        ArucoDict = new Dictionary(Dictionary.PredefinedDictionaryName.Dict4X4_50); // bits x bits (per marker) _ number of markers in dict
        #endregion

        #region Initialize Aruco parameters for markers detection
        ArucoParameters = new DetectorParameters();
        
        //ArucoParameters = DetectorParameters.GetDefault();
        ArucoParameters = SetArucoParameters();
        #endregion
    }

    // Update is called once per frame
    void Update()
    {
        if (_webcam.IsOpened)
        {
            _webcam.Grab();
        }
    }

    private void OnDestroy()
    {
        _webcam.Dispose();
        CvInvoke.DestroyAllWindows();
    }

    private void _handleWebcamQueryFrame(object sender, EventArgs e)
    {
        if (_webcam.IsOpened) _webcam.Retrieve(_webcamFrame);
        if (_webcamFrame.IsEmpty) return;
        FrameProcessing();
    }

    private void FrameProcessing()
    {
        _frameDisplay = _webcamFrame.Clone();
        //CvInvoke.Flip(_frameDisplay, _frameDisplay, FlipType.Horizontal);

        //_frameDisplay = filter(_frameDisplay);

        //_frameGray = _frameDisplay.Clone();

        //CvInvoke.CvtColor(_frameGray, _frameGray, ColorConversion.Bgr2Gray);
        //if (_frameGray.IsEmpty) return;

        //if (_adaptive_threshold)
        //    if (_adaptive_gaussian_threshold)
        //        CvInvoke.AdaptiveThreshold(_frameGray, _frameGray, _threshold_max_value, AdaptiveThresholdType.GaussianC, ThresholdType.BinaryInv, 2 * _threshold_bloc_size + 1, _threshold_param1);
        //    else
        //        CvInvoke.AdaptiveThreshold(_frameGray, _frameGray, _threshold_max_value, AdaptiveThresholdType.MeanC, ThresholdType.BinaryInv, 2 * _threshold_bloc_size + 1, _threshold_param1);
        //else
        //    CvInvoke.Threshold(_frameGray, _frameGray, _threshold_value, _threshold_max_value, ThresholdType.BinaryInv);

        //_frameGray = morphing(_frameGray);

        //if (_enableContours)
        //{
        //    VectorOfVectorOfPoint contourList = contours(_frameGray);

        //    contourList = findMarkers(contourList);

        //    //for (int i = 0; i < contourList.Size; i++)
        //    //    CvInvoke.DrawContours(_frameDisplay, contourList, i, new MCvScalar(0, 0, 255), 2);

        //    markerPerspectiveTransform(contourList);
        //}


        if (!_frameDisplay.IsEmpty)
        {
            ArucoParameters = SetArucoParameters();

            #region Detect markers on last retrieved frame
            VectorOfInt ids = new VectorOfInt(); // name/id of the detected markers
            VectorOfVectorOfPointF corners = new VectorOfVectorOfPointF(); // corners of the detected marker
            VectorOfVectorOfPointF rejected = new VectorOfVectorOfPointF(); // rejected contours
            ArucoInvoke.DetectMarkers(_frameDisplay, ArucoDict, corners, ids, ArucoParameters, rejected);

            VectorOfInt rejectedIds = new VectorOfInt(rejected.Size);
            if (rejectedIds.Size > 0)
            {
                
                ArucoInvoke.DrawDetectedMarkers(_frameDisplay, rejected, rejectedIds, new MCvScalar(0, 0, 255));
            }

            if (ids.Size > 0)
            {
                for (int i = 0; i < ids.Size; i++)
                {
                    Debug.Log(ids[i]);
                }

                #region Draw detected markers
                ArucoInvoke.DrawDetectedMarkers(_frameDisplay, corners, ids, new MCvScalar(255, 0, 255));
                #endregion
            }
            #endregion

        #region Display current frame plus drawings
        if (!_frameDisplay.IsEmpty) CvInvoke.Imshow(_window, _frameDisplay);
            #endregion
        }
    }

    private Mat filter(Mat img)
    {
        Mat imgFiltered = img.Clone();

        if (_activateMedianBlur)
            CvInvoke.MedianBlur(imgFiltered, imgFiltered, _medianBlurFactor * 2 + 1);

        if (_activateGaussianBlur)
        {
            int size = _gaussianBlurFactor * 2 + 1;
            CvInvoke.GaussianBlur(imgFiltered, imgFiltered, new Size(size, size), size * _gaussianBlurSigmaFactor);
        }

        return imgFiltered;
    }

    private Mat morphing(Mat img)
    {
        //Morphology
        Mat imgMorph = img.Clone();
        Mat structuringElement = CvInvoke.GetStructuringElement(ElementShape.Cross, new Size(_morphingOperationSize * 2 + 1, _morphingOperationSize * 2 + 1), new Point(-1, -1));
        //Image<Gray, byte> imageMorph = imageThreshold.Dilate(1);
        if (_morphingOpening)
        {
            CvInvoke.Erode(imgMorph, imgMorph, structuringElement, new Point(-1, -1), _morphingIteration, BorderType.Constant, new MCvScalar(0));
            CvInvoke.Dilate(imgMorph, imgMorph, structuringElement, new Point(-1, -1), _morphingIteration, BorderType.Constant, new MCvScalar(0));
        }
        if (_morphingClosing)
        {
            CvInvoke.Dilate(imgMorph, imgMorph, structuringElement, new Point(-1, -1), _morphingIteration, BorderType.Constant, new MCvScalar(0));
            CvInvoke.Erode(imgMorph, imgMorph, structuringElement, new Point(-1, -1), _morphingIteration, BorderType.Constant, new MCvScalar(0));
        }

        return imgMorph;
    }

    private VectorOfVectorOfPoint contours (Mat img)
    {
        VectorOfVectorOfPoint vectListContours = new VectorOfVectorOfPoint();
        Mat hierarchy = new Mat();
        CvInvoke.FindContours(img, vectListContours, hierarchy, RetrType.List, ChainApproxMethod.ChainApproxNone);

        return vectListContours;        
    }

    private VectorOfVectorOfPoint findMarkers (VectorOfVectorOfPoint contours)
    {
        VectorOfVectorOfPoint vectListMarkers = new VectorOfVectorOfPoint();
        
        for (int i = 0; i < contours.Size; i++)
        {
            //bool convex = CvInvoke.IsContourConvex(contours[i]);
            //double perimeter = CvInvoke.ArcLength(contours[i], true);

            double contourArea = CvInvoke.ContourArea(contours[i], false);
            double contourAreaProportion = contourArea / (_webcamFrame.Height * _webcamFrame.Width);
            if (contourAreaProportion < _minContourAreaProportion || contourAreaProportion > _maxContourAreaProportion)
                continue;

            //!!! MODIFY Height and Width (not the real side value of rotated rect)
            RotatedRect rect = CvInvoke.MinAreaRect(contours[i]);
            double squareShapeComparison = rect.Size.Width / rect.Size.Height;
            if (squareShapeComparison < (1.0 - _squareShapeTolerance) || squareShapeComparison > (1.0 + _squareShapeTolerance))
                continue;

            double areaComparison = contourArea / (rect.Size.Width * rect.Size.Height);
            if (areaComparison < (1.0 - _areaRectTolerance) || areaComparison > (1.0 + _areaRectTolerance))
                continue;


            vectListMarkers.Push(contours[i]);
        }

        //Filter grid (center are close)

        //for (int i = 0; i < vectListMarkers.size; i++)
        //{
        //    var moments = CvInvoke.Moments(contours[i]);
        //    int cx = (int)(moments.M10 / moments.M00);
        //    int cy = (int)(moments.M01 / moments.M00);
        //    Point center = new Point(cx, cy);
        //}

        return vectListMarkers;
    }

    private DetectorParameters SetArucoParameters()
    {
        DetectorParameters parameters = DetectorParameters.GetDefault();
        parameters.AdaptiveThreshConstant = _AdaptiveThreshConstant;
        parameters.AdaptiveThreshWinSizeMax = _AdaptiveThreshWinSizeMax;
        parameters.AdaptiveThreshWinSizeMin = _AdaptiveThreshWinSizeMin;
        parameters.AdaptiveThreshWinSizeStep = _AdaptiveThreshWinSizeStep;
        parameters.CornerRefinementMaxIterations = _CornerRefinementMaxIterations;
        //parameters.CornerRefinementMethod = DetectorParameters.RefinementMethod.Contour;
        parameters.CornerRefinementMinAccuracy = _CornerRefinementMinAccuracy;
        parameters.CornerRefinementWinSize = _CornerRefinementWinSize;
        parameters.ErrorCorrectionRate = _ErrorCorrectionRate;
        parameters.MarkerBorderBits = _MarkerBorderBits;
        parameters.MaxErroneousBitsInBorderRate = _MaxErroneousBitsInBorderRate;
        parameters.MaxMarkerPerimeterRate = _MaxMarkerPerimeterRate;
        parameters.MinCornerDistanceRate = _MinCornerDistanceRate;
        parameters.MinDistanceToBorder = _MinDistanceToBorder;
        parameters.MinMarkerDistanceRate = _MinMarkerDistanceRate;
        parameters.MinMarkerPerimeterRate = _MinMarkerPerimeterRate;
        parameters.MinOtsuStdDev = _MinOtsuStdDev;
        parameters.PerspectiveRemoveIgnoredMarginPerCell = _PerspectiveRemoveIgnoredMarginPerCell;
        parameters.PerspectiveRemovePixelPerCell = _PerspectiveRemovePixelPerCell;
        //parameters.PolygonalApproxAccuracyRate;
        return parameters;
    }

    private void markerPerspectiveTransform(VectorOfVectorOfPoint probablyMarkers)
    {
        if (probablyMarkers.Size <= 0)
            return;

        Mat tmp_frameDisplay = _frameDisplay.Clone();
        for (int i = 0; i < probablyMarkers.Size; i++)
        {
            RotatedRect rotRect = CvInvoke.MinAreaRect(probablyMarkers[i]);
            Rectangle roiRect = CvInvoke.BoundingRectangle(probablyMarkers[i]);

            //ROI : region of interest
            Mat roi = new Mat(tmp_frameDisplay, roiRect);
            CvInvoke.Resize(roi, roi, new Size(100, 100));
            if (i == 0)
            {
                _frameDisplay = roi.Clone();
            }
            else
            {
                CvInvoke.HConcat(_frameDisplay, roi, _frameDisplay);
            }

            //rotRect.GetVertices();
            //CvInvoke.GetPerspectiveTransform(roiRect.);
            //CvInvoke.WarpPerspective();
        }
    }

    /// <summary>
    /// Get the centroid of an object based on 4 coners.
    /// Such corners are easily obtained with Aruco markers.
    /// </summary>
    /// <param name="corner"> The 4 corners from which we would like to calculate the centroid. </param>
    /// <returns></returns>
    PointF GetCentroidFromCorner(VectorOfPointF corner)
    {
        PointF center = new PointF(0, 0);
        center.X = (corner[0].X + corner[1].X + corner[2].X + corner[3].X) / 4; //X is on horizontal axis = cols /!\ opencv Mat are row based (y,x) = (i,j), left to right, top to bottom
        center.Y = (corner[0].Y + corner[1].Y + corner[2].Y + corner[3].Y) / 4; //Y is on vertical axis = rows /!\ opencv Mat are row based (y,x) = (i,j), left to right, top to bottom
        return center;
    }
}
