﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Emgu.CV;
using Emgu.CV.Util;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System;
using System.Drawing;
using System.IO;

public static class ImgProcessingLib
{

    public static Mat FilterMedianBlur(ref Mat img, int factor)
    {
        Mat imgFiltered = img.Clone();
        CvInvoke.MedianBlur(imgFiltered, imgFiltered, factor * 2 + 1);
        return imgFiltered;
    }

    public static Mat FilterGaussianBlur(ref Mat img, int factor, float sigmaVariation)
    {
        Mat imgFiltered = img.Clone();

        int size = factor * 2 + 1;
        CvInvoke.GaussianBlur(imgFiltered, imgFiltered, new Size(size, size), size * sigmaVariation);

        return imgFiltered;
    }

    // Morphing opening operation is composed of two sub operation: erode and dialte (binary image mat)
    public static Mat MorphingOpening(ref Mat img, int elementSize, int iteration)
    {
        Mat imgMorph = img.Clone();
        Mat structuringElement = CvInvoke.GetStructuringElement(ElementShape.Cross, new Size(elementSize * 2 + 1, elementSize * 2 + 1), new Point(-1, -1));

        CvInvoke.Erode(imgMorph, imgMorph, structuringElement, new Point(-1, -1), iteration, BorderType.Constant, new MCvScalar(0));
        CvInvoke.Dilate(imgMorph, imgMorph, structuringElement, new Point(-1, -1), iteration, BorderType.Constant, new MCvScalar(0));

        return imgMorph;
    }

    // Morphing closing operation is composed of two sub operation: dialte and erode (binary image mat)
    public static Mat MorphingClosing(ref Mat img, int elementSize, int iteration)
    {
        Mat imgMorph = img.Clone();
        Mat structuringElement = CvInvoke.GetStructuringElement(ElementShape.Cross, new Size(elementSize * 2 + 1, elementSize * 2 + 1), new Point(-1, -1));

        CvInvoke.Dilate(imgMorph, imgMorph, structuringElement, new Point(-1, -1), iteration, BorderType.Constant, new MCvScalar(0));
        CvInvoke.Erode(imgMorph, imgMorph, structuringElement, new Point(-1, -1), iteration, BorderType.Constant, new MCvScalar(0));

        return imgMorph;
    }

    // Find contours from a binary image mat
    public static VectorOfVectorOfPoint Contours(ref Mat img)
    {
        VectorOfVectorOfPoint vectListContours = new VectorOfVectorOfPoint();
        Mat hierarchy = new Mat();
        CvInvoke.FindContours(img, vectListContours, hierarchy, RetrType.List, ChainApproxMethod.ChainApproxNone);

        return vectListContours;
    }

    // Find the biggest contour index, with a minArea, if there is one
    public static int? BiggestContourIndex(ref VectorOfVectorOfPoint contours, double minArea)
    {
        if (contours.Size <= 0)
            return -1;

        int? biggestContour = null;
        double biggestContourArea = 0;

        for (int i = 0; i < contours.Size; i++)
        {
            double contourArea = CvInvoke.ContourArea(contours[i], false);
            if (minArea < contourArea && biggestContourArea < contourArea)
            {
                biggestContour = i;
                biggestContourArea = contourArea;
            }
        }

        return biggestContour;
    }

    // Find the biggest contour, with a minArea, if there is one
    public static VectorOfPoint BiggestContour(ref VectorOfVectorOfPoint contours, double minArea)
    {
        VectorOfPoint contour = new VectorOfPoint();
        if (contours.Size <= 0)
            return contour;

        int? biggestContour = BiggestContourIndex(ref contours, minArea);

        if (biggestContour != null)
            contour = contours[(int)biggestContour];

        return contour;
    }

    // Find contour center form the moments of the contour
    public static Point ContourCenter(ref VectorOfPoint contour)
    {
        Point center = new Point();
        if (contour.Size <= 0)
            return center;
        
        var moments = CvInvoke.Moments(contour);
        int cx = (int)(moments.M10 / moments.M00);
        int cy = (int)(moments.M01 / moments.M00);
        center = new Point(cx, cy);

        return center;
    }

    // Returns a binary image from another image, keeping only parts that are between the 2 HSV values
    public static Mat ColorThresholdOnHSV(ref Mat imgHSV, Hsv hsvLowTrigger, Hsv hsvHighTrigger)
    {
        Image<Hsv, byte> imageHSV = imgHSV.ToImage<Hsv, byte>();      
        Image<Gray, byte> imageThreshold = imageHSV.InRange(hsvLowTrigger, hsvHighTrigger);
        return imageThreshold.Mat.Clone();
    }

    // A function to flip the corners of a rectangle (needed for marker detection, to match the display over the footage)
    public static VectorOfVectorOfPointF HorizontalFlipCorners(ref VectorOfVectorOfPointF corners, int width)
    {
        VectorOfVectorOfPointF flippedCorners = new VectorOfVectorOfPointF();

        for (int i = 0; i < corners.Size; i++)
        {
            VectorOfPointF corner = corners[i];
            List<PointF> flippedPoints = new List<PointF>();
            for (int j = 0; j < corner.Size; j++)
            {
                flippedPoints.Add(HorizontalFlipPoint(corner[j], width));
            }
            flippedCorners.Push(new VectorOfPointF(flippedPoints.ToArray()));
        }

        return flippedCorners;
    }

    public static PointF HorizontalFlipPoint(PointF point, int width)
    {
        return new PointF(width - point.X, point.Y);
    }

    // Translate a corner, with a vector offset (this is needed for marker detection, since the detection is on one half of the footage, to match the display over the footage)
    public static VectorOfVectorOfPointF OffsetCorners(ref VectorOfVectorOfPointF corners, PointF offset)
    {
        VectorOfVectorOfPointF flippedCorners = new VectorOfVectorOfPointF();

        for (int i = 0; i < corners.Size; i++)
        {
            VectorOfPointF corner = corners[i];
            List<PointF> flippedPoints = new List<PointF>();
            for (int j = 0; j < corner.Size; j++)
            {
                flippedPoints.Add(new PointF(corner[j].X + offset.X, corner[j].Y + offset.Y));
            }
            flippedCorners.Push(new VectorOfPointF(flippedPoints.ToArray()));
        }

        return flippedCorners;
    }

    // Convertion function from mat to texture, to display a mat in Unity world space
    public static Texture2D ConvertMatToTexture2D(ref Mat img, int width, int height)
    {
        if (img.IsEmpty) return new Texture2D(width, height);
        Mat mat = img.Clone();
        CvInvoke.Resize(mat, mat, new Size(width, height));

        if (mat.IsEmpty) return new Texture2D(width, height);
        CvInvoke.Flip(mat, mat, FlipType.Vertical); //The convention for textures is not the same, we need to flip the image vertically

        if (mat.IsEmpty) return new Texture2D(width, height);
        Texture2D texture = new Texture2D(width, height, TextureFormat.RGBA32, false);
        texture.LoadRawTextureData(mat.ToImage<Rgba, byte>().Bytes);
        texture.Apply();

        return texture;
    }
}
