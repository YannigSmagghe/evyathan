﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndManager : MonoBehaviour
{
	//instance of the singleton pattern
	public static EndManager instance;
	
	//objects that the endscene will interact with
	public Camera cam;
	public GameObject jewelryFall;
	public GameObject sword;
	public GameObject emguLib;

    void Awake()
    {
		//Singleton pattern
		if (instance == null)
			instance = this;
		else if (instance != this)
			Destroy(this.gameObject);
    }

	//coroutine that manage the end scene
	public IEnumerator EndSceneCoroutine()
	{
		sword.SetActive(false);
		emguLib.GetComponent<ImgProcessing>()._enable_color_detection = false;
		emguLib.GetComponent<ImgProcessing>()._enable_marker_detection = false;
		jewelryFall.SetActive(true);
		Animation anim = cam.GetComponent<Animation>();
		anim.Play();
		yield return new WaitForSeconds(23.4f);
		SceneManager.LoadScene(2);
	}
}
