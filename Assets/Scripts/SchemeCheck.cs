﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SchemeCheck : MonoBehaviour
{
	//to check if the point has been activated
    private bool schemeChecked = false;

	//parent Scheme the point belongs to
    private Scheme scheme;

	void Start()
    {
		scheme = transform.parent.GetComponent<Scheme>();
       
		gameObject.SetActive(false);
		schemeChecked = false;
    }
	
    void OnTriggerEnter(Collider other)
    {
		//if the sword touch the point, if he's the next one and not activated yet, active it
        if (gameObject.activeInHierarchy && !schemeChecked && other.CompareTag("Sword"))
        {
			scheme.currentIndex = scheme.currentIndex + 1;
			GetComponent<MeshRenderer>().material = scheme.checkedMat;
			schemeChecked = true;
        }

    }
}
