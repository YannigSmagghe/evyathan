# Evyathan
Evyathan is a funny game based on webcam control.
You can slay monsters, resolve puzzles, and activate spells to win the treasure!

# Emgucv
We use EmguCV librairy for image processing with Unity.
We implemented several image processing features, like:
- color detection: you have to make yourself a green sword to play
- marker detection: you have 4 markers correspondinig to 4 spells
- cat face detection: this is a secret spell, so secret that it is not even implemented (the detection works, but we did not have the time to create the cat sound effects and the rainbow trail)

We also did so improvement to improve the frame rate in game.
For example, the marker detection and the cat face detection are only checked in the left half of the webcam image.
(We supposed the player was right-handed, sorry)

# Gameplay
You can use your green sword to slay enemies. 
Some enemies are bigger than others, so be careful...
In this railshooter, you can choose your own path through crosses, the choice will appear on screen.
To choose your path, just point your sword in the desired direction.
Another function is to cast spells with markers (this is especially useful when there is a lot of enemies in a corridor).
You have four spells, you can use a spell only once.
Your sword is not only useful to slay, but also to resolve puzzles.
At the end of the game, unlock the puzzles to win the treasure!

# Dev
- Eva Gerbert-Gaillard: https://www.linkedin.com/in/eva-gerbert-gaillard-64341415b/ & https://evagerbertgaillard.wixsite.com/portfolio
- Nathan Desages: nathan.desages@gmail.com & https://github.com/Raitozan
- Yannig Smagghe: https://www.linkedin.com/in/yannig-smagghe-b18a30119/ & https://gitlab.com/YannigSmagghe

# Special Thanks	
- Oceane, Axel, and Takie for dancing in the trailer

## Trailer
https://youtu.be/3H7E4ZkFnIQ

## Source
https://gitlab.com/YannigSmagghe/evyathan
